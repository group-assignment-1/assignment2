import ballerina/graphql;


type Department_objectivesReq record {|
    int objects_id;
    string name;
    string description;
    int percentageWeight;
|};

table<Department_objectives> key (objects_id) objectsTable = table [];

@graphql:ServiceConfig {
    graphiql: {
        enabled: true

    }
}
service / on new graphql:Listener(3000) {

    resource function get getAllObjectives() returns table<Department_objectives> {
        return objectsTable;
    }

   remote function addObjective(Department_objectivesReq newObjective) returns string {
        var  {objects_id, ...data} = newObjective;
        objectsTable.add({objects_id: objects_id, ...data});
        objectsTable.add({objects_id: newObjective.objects_id, name:newObjective.name,description:newObjective.description,percentageWeight:newObjective.percentageWeight});
        return newObjective.name + "saved successfuly";
    }
   

  
}
