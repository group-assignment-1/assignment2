//import ballerina/http;
import ballerina/sql;
import ballerinax/mysql;
import ballerinax/mysql.driver as _;


type Department_objectives readonly & record {|
   readonly int objects_id;
    string name;
    string description;
    int percentageWeight;
|};

public distinct service class fciData{
//service /graphql on new graphql:Listener(3000) {
   private final mysql:Client db;

   function init() returns error? {
       // Initiate the mysql client at the start of the service. This will be used
         //throughout the lifetime of the service.
    self.db = check new ("localhost", "root", "P@ssword258!", "fci_pms", 3307);
    }

     resource function get objectives() returns Department_objectives[]|error {
      
        stream<Department_objectives, sql:Error?> objectiveStream = self.db->query(`SELECT * FROM department_object`);

        return from Department_objectives objectives in objectiveStream
            select objectives;
}
    resource function post objectives(Department_objectives objectives) returns Department_objectives|error {
        _ = check self.db->execute(`
            INSERT INTO department_object (objects_id, name, description, percentageWeight)
            VALUES (${objectives.objects_id}, ${objectives.name}, ${objectives.description}, ${objectives.percentageWeight});`);
        return objectives;
    }
}

