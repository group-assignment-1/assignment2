import ballerinax/mongodb;
import ballerina/graphql;
import ballerina/io;

type Employee record {
    string id;
    string firstName;
    string lastName;
    string jobTitle;
    string departmentId;
    
};

type Department record {
    string id;
    string name;
    
};

type DepartmentObjective record {
    string id;
    string departmentId;
    string description;
    int weightage;
   
};
// Create a MongoDB client for the Employees collection.
mongodb:ConnectionConfig mongoConfig = {
    connection: {
        host: "localhost",
        port: 21017, //from mongodb compass
        auth: {
            username: "",
            password: ""
        },
        options: {
            sslEnabled: false, 
            serverSelectionTimeout: 5000
        }
    },
    databaseName: "courseManagement"
};

mongodb:Client mongoClient = check new (mongoConfig); //its not working 
configurable string collectionName = "courses";

configurable string employeeCollectionName = "employees";
configurable string departmentCollectionName = "departments";
configurable string departmentObjectiveCollectionName = "departmentObjectives";

type QueryObject record {
    
};

type MutationObject record {
    
};

type DepartmentObjectiveInput record {
    //string departmentRecord;
    
};

type EmployeeInput record {
    
};

type bool record {
    
};

type DepartmentInput record {
    
};

type createDeartment record {|
    
|};

service /performanceManagement on new graphql:Listener(8080) {

//resource function is the queries

resource function QueryObject Query/employees() returns json[] {
        json[] employeesRecords = getEmployees();
        return employeesRecords; //<QueryObject>employeesRecords;  //still does not want to work
    }

    //this line queries the operation name with an argument id of type string 
    // handles queries for employee data
    //QueryObject type is used to specify the return type of this operation.
    resource function QueryObject Query/employee(string id) returns error|json {  
        //This line invokes the getEmployeeById(id) function,
        // which retrieves an employee record based on the provided id. 
        //It assigns the result to the variable employeeRecord. 
        //The json? type indicates that employeeRecord can be either a JSON record or null if the employee is not found in the database.
        json? employeeRecord = getEmployeeById(id); 
//This line checks if employeeRecord is of type json. If it is, that means a valid employee record was found in the database.
        if (employeeRecord is json) {
            //wrapping the employeeRecord in a <QueryObject> type
            //is how the data is returned to the GraphQL client
            return employeeRecord;//(QueryObject)employeeRecord; //help....uncomment this one 
        } else { //return an empty message
            return ("Error");//new graphql:Error("error");  //help  its supposed return an error message
        }
    }


//department
resource function QueryObject Query/departments() returns json[] {
        json[] departmentRecords = getDepartments(); //get function for department
        return departmentRecords; //<QueryObject>departmentRecords;
    }
//handles GraphQL queries for retrieving department information
//id is a unique identifier
    resource function QueryObject Query/department(string id) returns error|json {
        
        json? departmentRecord = getDepartmentById(id);
        if (departmentRecord is json) { //
            return departmentRecord ;//(QueryObject)departmentRecord;  //uncomment this one too
        } else {
            return ("Error");//new graphql:Error("Department not found");  //give an error message
        }
    }

//
    resource function QueryObject Query/departmentObjectives(string departmentId) returns error| json {
        json[]? objectivesRecords = getDepartmentObjectivesByDepartmentId(departmentId);
        if (objectivesRecords is json[]) {
             return objectivesRecords;//(<QueryObject>)(objectivesRecords);  //uncomment this one also
        } else {
            return ("No objectives found for the department");//new graphql:Error("No objectives found for the department");
        }
    }

    resource function MutationObject Mutation/createDepartmentObjective(DepartmentObjectiveInput input) returns MutationObject {
        json newObjectiveRecord = createDepartmentObjective(input);
        return <MutationObject>newObjectiveRecord;
    }

    // Implement other queries and mutations

    // Helper functions for MongoDB operations
//remote function are Mutations

// GraphQL mutation resource functions

//creating the employee
    resource function MutationObject Mutation/createEmployee(EmployeeInput input) returns error| json {
        json newEmployeeRecord = createEmployee(input);
        return newEmployeeRecord; //<MutationObject>newEmployeeRecord;
    }
//delete employee
resource function MutationObject Mutation/deleteEmployee(string id) returns () | MutationObject {
        bool success = deleteEmployee(id);
        if (success) { //why is it not picking...its success
            return MutationObject; //<MutationObject>{"message": "Employee deleted successfully"};
        } else {
            return (); //new graphql:Error("Employee not found or couldn't be deleted");
        }
    }


    resource function MutationObject Mutation/createDepartment(DepartmentInput input) returns () {
        json newDepartmentRecord = createDepartment(input);
        return ();//createDepartment;//<MutationObject>newDepartmentRecord;
    }

    resource function MutationObjects Mutation/createDepartmentObjective(DepartmentObjectiveInput input) {
        json newObjectiveRecord = createDepartmentObjective(input);
        return ();//<MutationObject>newObjectiveRecord;
    }

    // Helper functions for MongoDB operations
    //this function 
    function getEmployees() returns json[] {
        return mongoClient->find(employeeCollectionName, {}); //supposed to read from the client
    }

    function getEmployeeById(string id) returns json? {
        var result = mongoClient->findOne(employeeCollectionName, check <json> { "_id": id }); //the find operation does not want to work 
        if (result is json) {
            return result;
        } else {
            return ();
        }
    }
function createEmployee(EmployeeInput input) returns json {
    // Generate a unique ID for the new employee.
    input.id = generateEmployeeId();

    // Convert the input to a JSON document.
    json employeeDocument = input.toString();

    // Insert the new employee document into the MongoDB collection.
    var result = mongoClient->insertOne(employeeCollectionName, check <json>employeeDocument);

    if (result is json) {
        return input;
    } else {
        return json {};
    }
}

function updateEmployee(string id, EmployeeInput input) returns json {
    // Convert the input to a JSON document and exclude the ID field.
    json employeeDocument = input.toString();
    employeeDocument.remove("id");

    // Define the filter condition to find the employee by ID.
    json filter = <json> { "_id": id };

    // Update the corresponding employee record in the MongoDB collection.
    var result = mongoClient->updateOne(employeeCollectionName, filter, employeeDocument);

    if (result is json) {
        return input;
    } else {
        return json {};
    }
}

function deleteEmployee(string id) returns boolean {
    // Define the filter condition to find the employee by ID.
    json filter = check <json> { "_id": id };

    // Delete the employee record from the MongoDB collection.
    var result = mongoClient->deleteOne(employeeCollectionName, filter);

    return result is json;
}

function generateEmployeeId() returns string {
    // Generate a unique ID using a timestamp and a random number.
    int timestamp = math:floor(math:now());
    int randomNumber = math:rand(1000, 9999);
    return "employee-" + timestamp + "-" + randomNumber;
}

function createDepartment(DepartmentInput input) returns json {
    // Generate a unique ID for the new department.
    input.id = generateDepartmentId();
    
    // Convert the input to a JSON document.
    json departmentDocument = input.toString();
    
    // Insert the new department document into the MongoDB collection.
    var result = mongoClient->insertOne(departmentCollectionName, check <json> departmentDocument);
    
    if (result is json) {
        return input;
    } else {
        return json {};
    }
}

function createDepartmentObjective(DepartmentObjectiveInput input) returns json {
    // Generate a unique ID for the new department objective.
    input.id = generateDepartmentObjectiveId();
    
    // Convert the input to a JSON document.
    json departmentObjectiveDocument = input.toString();
    
    // Insert the new department objective document into the MongoDB collection.
    var result = mongoClient->insertOne(departmentObjectiveCollectionName, check <json> departmentObjectiveDocument);
    
    if (result is json) {
        return input;
    } else {
        return json {};
    }
}

function generateDepartmentId() returns string {
    // Generate a unique ID using a timestamp and a random number.
    int timestamp = math:floor(math:now());
    int randomNumber = math:rand(1000, 9999);
    return "department-" + timestamp + "-" + randomNumber;
}

function generateDepartmentObjectiveId() returns string {
    // Generate a unique ID using a timestamp and a random number.
    int timestamp = math:floor(math:now());
    int randomNumber = math:rand(1000, 9999);
    return "departmentObjective-" + timestamp + "-" + randomNumber;
}
}

function deleteEmployee(string s) returns bool {
    return {};
}

function createEmployee(EmployeeInput r) returns json {
    return ();
}

function createDepartmentObjective(DepartmentObjectiveInput r) returns json {
    return ();
}

function getDepartmentObjectivesByDepartmentId(string s) returns json[]? {
    return ();
}

function getDepartmentById(string s) returns json? {
    return ();
}

function getDepartments() returns json[] {
    return [];
}

function getEmployeeById(string s) returns json? {
    return ();
}

function getEmployees() returns json[] {
    return [];
}
