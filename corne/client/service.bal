import ballerina/io;
import ballerina/grpc;
import ballerina/http;
import ballerina/graphql;

service client {

    public resource getEmployeeScores(string employeeId) {

        string query = "query { getEmployeeScores(employeeId: \"" + employeeId + "\") { scores } }";

        graphql:Response response = graphql:execute(query, "http://localhost:8080/graphql");

        if (response.status == graphql:Status.SUCCESS) {
            list<EmployeeScore> scores = response.data.getEmployeeScores;
            return scores;
        } else {
            error(response.error.message);
        }
    }
}