import ballerina/io;
import ballerina/grpc;
import ballerina/http;
import ballerina/graphql;

service server on graphql:Listener(8080) {
    graphql:Schema schema = {
        query: {
            getEmployeeScores(string employeeId): [EmployeeScore] {
                return employeeScores.get(employeeId);
            }
        }
    };
    resource getEmployeeScores(string employeeId) {
        return employeeScores.get(employeeId);
    }
}