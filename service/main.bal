import ballerina/graphql;
import ballerina/sql;
import ballerinax/mongodb;

type Employee record{
    string name;
    string lastname;
    string kpi;
    string jobTitle;
    string position;

};

type kpi record{
    string peerRecognistion;
    string professionalDevelopment;
    string studentProgress;
    string innovativeTeaching;
    string researchOutput; 
};

service / on new graphql:Listener(9090) {
private mongodb:Client.db;
function init(){
    self.db = check new ("localhost", "root",
    "Test@321", "EmployeeManagement", 9011);
}
resource function get getAllEmployeeProgress() returns kpi[] | error{
    stream<Employee, mongodb:Error?> employeeStream = self.db->query(SELECT * From Employee);
}
return from Employee employee in employeeStream Select Employee;


}
public function main() {

    mongodb:ClientConfig mongoConfig = {
        host: "localhost",
        port: 9090,
        username: "admin",
        password: "admin",
        options: {sslEnabled: false, serverSelectionTimeout: 5000}
    };

    mongodb:Client mongoClient = checkpanic new (mongoConfig, "Ballerina");

    map<json> doc1 = { "name": "ballerina", "type": "src" };
    map<json> doc2 = { "name": "connectors", "type": "artifacts" };
    map<json> doc3 = { "name": "docerina", "type": "src" };
    map<json> doc4 = { "name": "test", "type": "artifacts" };

    log:printInfo("------------------ Inserting Data -------------------");
    checkpanic mongoClient->insert(doc1,"projects");
    checkpanic mongoClient->insert(doc2,"projects");
    checkpanic mongoClient->insert(doc3,"projects");
    checkpanic mongoClient->insert(doc4,"projects");
  
    log:printInfo("------------------ Counting Data -------------------");
    int count = checkpanic mongoClient->countDocuments("projects",());
    log:printInfo("Count of the documents '" + count.toString() + "'.");


    log:printInfo("------------------ Querying Data -------------------");
    map<json>[] jsonRet = checkpanic mongoClient->find("projects",(),());
    log:printInfo("Returned documents '" + jsonRet.toString() + "'.");

    map<json> queryString = {"name": "connectors" };
    jsonRet = checkpanic mongoClient->find("projects", (), queryString);
    log:printInfo("Returned Filtered documents '" + jsonRet.toString() + "'.");


    log:printInfo("------------------ Updating Data -------------------");
    map<json> replaceFilter = { "type": "artifacts" };
    map<json> replaceDoc = { "name": "main", "type": "artifacts" };

    int response = checkpanic mongoClient->update(replaceDoc,"projects", (), replaceFilter, true);
    if (response > 0 ) {
        log:printInfo("Modified count: '" + response.toString() + "'.") ;
    } else {
        log:printInfo("Error in replacing data");
    }

   log:printInfo("------------------ Deleting Data -------------------");
   map<json> deleteFilter = { "name": "ballerina" };
   int deleteRet = checkpanic mongoClient->delete("projects", (), deleteFilter, true);
   if (deleteRet > 0 ) {
       log:printInfo("Delete count: '" + deleteRet.toString() + "'.") ;
   } else {
       log:printInfo("Error in deleting data");
   }

     mongoClient->close();
}